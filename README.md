# Wine Library

This is rejected project, all source code are rewriting into Wine-new because of dumb code
This is group project for XML course in FPT University.
This app use Node.js using ExpressJS 4.0 framework.
Front-end use Angular.js with Bootstrap.

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) installed.

```sh
git clone https://FongZooZ@bitbucket.org/FongZooZ/wine.git
cd Wine
npm install
npm start
```

Your app should now be running on [localhost:1337](http://localhost:1337/).

## Dependencies
* Express.js: http://expressjs.com
* async: https://github.com/caolan/async
* normalizeurl: https://www.npmjs.com/package/normalizeurl
* xml2js: https://www.npmjs.com/package/xml2js

* Angular.js: https://angularjs.org
* Bootstrap: http://getbootstrap.com
* JQuery: http://jquery.com
* WOW.js: https://github.com/matthieua/WOW
* Sortable: http://github.hubspot.com/sortable/docs/welcome