function DashboardController($scope, $rootScope) {
	$scope.wines = [];
	$scope.wineTypes = [];
	$scope.volumes = [];
	$scope.countries = [];
	$scope.wineMakers = [];

	var wines = {};
	wines.wines = {};
	wines.wines.wine = [];

	var wineTypes = {};
	wineTypes.wineTypes = {};
	wineTypes.wineTypes.wineType = [];

	var volumes = {};
	volumes.volumes = {};
	volumes.volumes.volume = [];

	var countries = {};
	countries.countries = {};
	countries.countries.country = [];

	var wineMakers = {};
	wineMakers.wineMakers = {};
	wineMakers.wineMakers.wineMaker = [];

	$scope.newWines = {};
	$scope.newWineTypes = {};
	$scope.newVolumes = {};
	$scope.newCountries = {};
	$scope.newWineMakers = {};

	$rootScope.$on("xml.added", function(mass) {
		loadAllXML();
	});

	$scope.checkClickAddNewVolume = function() {
		$scope.newVolumes.$ = {}
		$scope.newVolumes.volumeID = $scope.volumes.length + 1;
	}

	// add
	$scope.addWineType = function() {
		wineTypes.wineTypes.wineType = $scope.wineTypes;
		wineTypes.wineTypes.wineType.push($scope.newWineTypes);
		$.ajax({
			url: '/addWineType',
			method: 'post',
			data: angular.fromJson(angular.toJson(wineTypes)),
			success: function(result) {
				$scope.newWineTypes = {};
				$rootScope.$emit("wineType.added");
				$rootScope.$emit("xml.added");
			}
		});
		$("#add-new-type").modal("hide");
	}

	$("#add-new-type").on('hidden.bs.modal', function() {
		$scope.newWineTypes = {};
	});

	$scope.addVolume = function() {
		volumes.volumes.volume = $scope.volumes;
		volumes.volumes.volume.push($scope.newVolumes);
		$.ajax({
			url: '/addVolume',
			method: 'post',
			data: angular.fromJson(angular.toJson(volumes)),
			success: function(result) {
				$scope.newVolumes = {};
				$rootScope.$emit("volume.added");
				$rootScope.$emit("xml.added");
			}
		});
		$("#add-new-volume").modal("hide");
	}

	$("#add-new-volume").on('hidden.bs.modal', function() {
		$scope.newVolumes = {};
	});

	$scope.addCountry = function() {}

	// edit
	$scope.editWine = function(wineName, wineMakerID) {}

	function loadWines() {
		$.ajax({
			url: '/loadWines',
			success: function(wines) {
				$scope.$apply(function() {
					$scope.wines = wines.wines.wine;
				});
			}
		});
	}

	loadAllXML();

	function loadAllXML() {
		loadWines();
		loadWineTypes();
		loadVolumes();
		loadCountries();
		loadWineMakers();
	}

	function loadWineTypes() {
		$.ajax({
			url: '/loadWineTypes',
			success: function(wineTypes) {
				$scope.$apply(function() {
					$scope.wineTypes = wineTypes.wineTypes.wineType;
				});
			}
		});
	}

	function loadVolumes() {
		$.ajax({
			url: '/loadVolumes',
			success: function(volumes) {
				$scope.$apply(function() {
					$scope.volumes = volumes.volumes.volume;
				});
			}
		});
	}

	function loadCountries() {
		$.ajax({
			url: '/loadCountries',
			success: function(countries) {
				$scope.$apply(function() {
					$scope.countries = countries.countries.country;
				});
			}
		});
	}

	function loadWineMakers() {
		$.ajax({
			url: '/loadWineMakers',
			success: function(wineMakers) {
				$scope.$apply(function() {
					$scope.wineMakers = wineMakers.wineMakers.wineMaker;
				});
			}
		});
	}
}