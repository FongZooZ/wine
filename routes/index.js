/* GET home page. */
var ParseService = require('../services/ParseService.js');
var WineDetailServices = require('../services/WineDetailServices.js');


exports.index = function(req, res) {
	WineDetailServices.getWineDetail("Sirus Cherrya");
	// WineDetailServices.getDetailCountry("FR");

	ParseService.toJS('/../public/xml/wine.xml', function(err, wines) {
		res.render('index', {
			wineInfo: wines
		});
	});
};