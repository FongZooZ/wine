var fs = require('fs');
var AdminService = require('../services/AdminService.js');
var ParseService = require('../services/ParseService.js');

exports.index = function(req, res, next) {
	if (!req.session.user) {
		res.redirect('login');
	} else {
		res.render('admin/dashboard');
	}
};

exports.login = function(req, res, next) {
	res.render('login');
};

exports.checkLogin = function(req, res, next) {
	var userData = req.body;
	AdminService.checkLogin(userData.username || "", userData.password || "", function(err, canLogin) {
		if (err) {
			return next(err);
		}
		if (canLogin) {
			var user = canLogin;
			req.session.user = user;
			req.session.user.password = "trustme,thiscanneverbearealpassword,hahahah";
			res.redirect("/admin");
		} else {
			req.session.error = "Invalid username or password";
			res.redirect("/login");
		}
	});
};

exports.logout = function(req, res, next) {
	req.session.destroy();
	res.redirect('/');
};

exports.loadWines = function(req, res, next) {
	ParseService.toJS('/../public/xml/wine.xml', function(err, wines) {
		if (err) {
			console.log(err);
		}
		res.send(wines);
	});
};

exports.loadWineTypes = function(req, res, next) {
	ParseService.toJS('/../public/xml/type.xml', function(err, wineTypes) {
		if (err) {
			console.log(err);
		}
		res.send(wineTypes);
	});
};

exports.loadVolumes = function(req, res, next) {
	ParseService.toJS('/../public/xml/volume.xml', function(err, volumes) {
		if (err) {
			console.log(err);
		}
		res.send(volumes);
	});
};

exports.loadCountries = function(req, res, next) {
	ParseService.toJS('/../public/xml/country.xml', function(err, countries) {
		if (err) {
			console.log(err);
		}
		res.send(countries);
	});
};

exports.loadWineMakers = function(req, res, next) {
	ParseService.toJS('/../public/xml/winemaker.xml', function(err, wineMakers) {
		if (err) {
			console.log(err);
		}
		res.send(wineMakers);
	});
};

exports.wineType = function(req, res, next) {
	if (!req.session.user) {
		res.redirect('login');
	} else {
		res.render('admin/wineType');
	}
};

exports.addWineType = function(req, res, next) {
	var wineTypeData = req.body;
	if (!req.session.user) {
		res.redirect('login');
	} else {
		AdminService.addWineType(wineTypeData);
		res.send(200);
	}
};

exports.volume = function(req, res, next) {
	if (!req.session.user) {
		res.redirect('login');
	} else {
		res.render('admin/volume');
	}
};

exports.addVolume = function(req, res, next) {
	var volumeData = req.body;
	if (!req.session.user) {
		res.redirect('login');
	} else {
		AdminService.addVolume(volumeData);
		res.send(200);
	}
};

exports.country = function(req, res, next) {
	if (!req.session.user) {
		res.redirect('login');
	} else {
		res.render('admin/country');
	}
}

exports.addCountry = function(req, res, next) {
	// TODO
};

exports.wineMaker = function(req, res, next) {
	if (!req.session.user) {
		res.redirect('login');
	} else {
		res.render('admin/wineMaker');
	}
};