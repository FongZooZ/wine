var ParseService = require('./ParseService.js');
var async = require('async');

var getDetailWineMakerById = function getDetailById(id, cb) {
	ParseService.toJS('/../public/xml/winemaker.xml', function(err, data) {
		if (err) {
			cb(err);
		}
		var wineMaker = {};
		// console.log(data.wineMakers.wineMaker[0].$);
		for (var i = 0; i < data.wineMakers.wineMaker.length; i++) {
			var tmp = data.wineMakers.wineMaker[i];
			if (tmp.$.id == id) {
				wineMaker.$.id = id;
				wineMaker.wineMakerName = tmp.wineMakerName;
				wineMaker.countryID = tmp.countryID;
				break;
			}
		}
		// console.log(wineMaker);
		cb(err, wineMaker);

	});
}

var getDetailCountry = function getDetailCountry(id, cb) {

	ParseService.toJS('/../public/xml/country.xml', function(err, data) {
		if (err) {
			cb(err);
		}
		var country = {};
		for (var i = 0; i < data.countries.country.length; i++) {
			var tmp = data.countries.country[i];
			if (tmp.$.id == id) {
				country.$ = {};
				country.$.id = tmp.$.id;
				country.$.countryName = tmp.$.countryName;
				country.$.flagImg = tmp.$.flagImg;;
				break;
			}
		}
		cb(err, country);
	});
}

var getDetailVolumePrice = function getDetailVolumePrice(id, cb) {

	ParseService.toJS('/../public/xml/volume.xml', function(err, data) {
		if (err) {
			cb(err);
		}
		var volume = {};
		for (var i = 0; i < data.volumes.volume.length; i++) {
			var tmp = data.volumes.volume[i];

			if (tmp.volumeID == id) {
				volume = tmp;
			}
		}
		cb(err, volume);
	});
}

//TODO
// var getDetailVolumesPrices = function getDetailVolumesPrices(idList, cb) {

// 	ParseService.toJS('/../public/xml/volume.xml', function(err, data) {
// 		if (err) {
// 			cb(err);
// 		}
// 		var volumes = [];
// 		var volume = {};
// 		var count = 0;
// 		for (var i = 0; i < data.volumes.volume.length; i++) {
// 			var tmp = data.volumes.volume[i];

// 			if (tmp.volumeID == idList[count]) {
// 				volume = tmp;
// 				volumes.push(volume);
// 				count++;
// 			}
// 			if (count == idList.length) {
// 				break;
// 			}
// 		}
// 		cb(err, volumes);
// 	});
// }

var getDetailWineByName = function getDetailWineByName(name, cb) {

	ParseService.toJS('/../public/xml/wine.xml', function(err, data) {
		if (err) {
			cb(err);
		}
		var wine = {};
		for (var i = 0; i < data.wines.wine.length; i++) {
			var tmp = data.wines.wine[i];
			if (tmp.name == name) {
				wine.name = tmp.name;
				wine.imgSrc = tmp.imgSrc;
				wine.color = tmp.color;
				wine.type = tmp.type;
				wine.characteristic = tmp.characteristic;
				wine.description = tmp.description;
				wine.priceByVolume = tmp.priceByVolume;
				wine.rating = tmp.rating;
				wine.winemaker = {};
				wine.winemaker.$ = {};
				wine.winemaker.$.id = tmp.wineMakerID;
				break;
			}
		}
		cb(err, wine);
	});
}

var getWineDetail = function getWineDetail(name) {
	var wine = {};
	async.series([
		function(cb) {
			getDetailWineByName(name, function(err, data) {
				if (err) {
					cb(err);
				}
				wine = data;

				cb();
			});
		},
		function(cb) {
			if (!wine.winemaker) {
				cb();
			} else {
				getDetailWineMakerById(wine.winemaker.wineMakerID, function(err, wineMaker) {
					wine.winemaker.wineMakerName = wineMaker.wineMakerName;
					wine.winemaker.country = wineMaker.countryID;
					cb();
				});
			}
		},
		function(cb) {
			if (!wine.winemaker) {
				cb();
			} else {
				getDetailCountry(wine.winemaker.country, function(err, country) {
					wine.country = country;
					cb();
				});
			}
		},
		function(cb) {
			if (!wine.priceByVolume) {
				console.log("dcm");
				cb();
			} else if (!wine.priceByVolume.priceTag.length) {
				getDetailVolumePrice(wine.priceByVolume.priceTag.volumeID, function(err, volume) {
					wine.priceByVolume.priceTag.volume = volume;
					cb();
				});
			} else {
				cb();
			}

		},
		function(cb) {
			if (wine.winemaker) {
				return cb(null, wine);
			}else{
				return cb(new Error("no wine"), wine);
			}
		}
	]);


}

module.exports = {
	getDetailWineMakerById: getDetailWineMakerById,
	getDetailWineByName: getDetailWineByName,
	getWineDetail: getWineDetail,
	getDetailCountry: getDetailCountry,
	getDetailVolumePrice: getDetailVolumePrice
}