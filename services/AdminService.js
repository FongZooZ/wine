var fs = require('fs');
var crypto = require('crypto');
var ParseService = require('./ParseService.js');

var checkLogin = function checkLogin(username, password, callback) {
  readXmlAdminFile(function(err, data) {
    if (err) {
      return callback(err);
    }
    for (var i = 0; i < data.admins.admin.length; i++) {
      var md5 = data.admins.admin[i].password;
      var loginName = data.admins.admin[i].adminName.trim();
      if (loginName.toLowerCase() === username.trim().toLowerCase()) {
        // checkPassword(password, md5) ? return callback(null, true) : return callback(null, false);
        if (checkPassword(password, md5)) {
          return callback(null, true);
        } else {
          return callback(null, false);
        }
      }
    }
  });
}

var readXmlAdminFile = function readXmlAdminFile(callback) {
  ParseService.toJS('/../public/xml/admin.xml', callback);
}

var checkPassword = function checkPassword(password, md5) {
  return hash(password) == md5;
}

var hash = function hash(string) {
  string = !string ? "" : string;
  var md5 = crypto.createHash('md5');
  return md5.update(string).digest('hex');
}

var checkUsernameAvailability = function checkUsernameAvailability(username, callback) {

  if (err) {
    return callback(err);
  }
  if (user) {
    callback(null, false);
  } else {
    callback(null, true);
  }
}

var addWineType = function addWineType(wineTypeData) {
  fs.writeFile(__dirname + '../../public/xml/type.xml', ParseService.toXML(wineTypeData), function(err) {
    if (err) throw err;
    console.log('WineType saved!');
  });
};

var addVolume = function addVolume(volumeData) {
  fs.writeFile(__dirname + '../../public/xml/volume.xml', ParseService.toXML(volumeData), function(err) {
    if (err) throw err;
    console.log('Volume save!');
  });
}

module.exports = {
  checkLogin: checkLogin,
  addWineType: addWineType,
  addVolume: addVolume
};