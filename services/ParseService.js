var fs = require('fs');
var xml2js = require('xml2js');
var builder = new xml2js.Builder();

var toJS = function toJS(xmlPath, cb) {
	var parser = new xml2js.Parser({
		explicitArray: false,
		
	});

	fs.readFile(__dirname + xmlPath, function(err, data) {
		if (err) {
			cb(err);
			return;
		}
		parser.parseString(data, function(err, result) {
			return cb(err, result);
		});
	});
}

var toXML = function toXML(jsObject) {
	return builder.buildObject(jsObject);
}

module.exports = {
	toJS: toJS,
	toXML: toXML
}