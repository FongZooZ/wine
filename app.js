var debug = require('debug')('my-application');

var express = require('express');
var http = require('http');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes');
var users = require('./routes/user');
var admin = require('./routes/admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.cookieParser('secret code!'));
app.use(express.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

app.all("*", function(req, res, next) {
  if (req.session.error) {
    res.locals({
      error: req.session.error
    });
    req.session.error = null;
  }
  next();
});

app.get('/', routes.index);
app.get('/users', users.list);

app.set('port', process.env.PORT || 1337);

app.get('/admin', admin.index);
app.get('/login', admin.login);
app.post('/login', admin.checkLogin);
app.get('/logout', admin.logout);

// load all xml data
app.get('/loadWines', admin.loadWines);
app.get('/loadWineTypes', admin.loadWineTypes);
app.get('/loadVolumes', admin.loadVolumes);
app.get('/loadCountries', admin.loadCountries);
app.get('/loadWineMakers', admin.loadWineMakers);

// admin pages
app.get('/admin/wineType', admin.wineType);
app.get('/admin/volume', admin.volume);
app.get('/admin/country', admin.country);
app.get('/admin/wineMaker', admin.wineMaker);

app.post('/addWineType', admin.addWineType);
app.post('/addVolume', admin.addVolume);
app.post('/addCountry', admin.addCountry); // TODO


var server = app.listen(app.get('port'), function() {
  debug('Express server listening on port ' + server.address().port);
});

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//   res.render('error', {
//     message: err.message,
//     error: {}
//   });
// });

module.exports = app;